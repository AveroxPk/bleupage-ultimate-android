package com.example.sarimahmed.bleupage.Adapter

/**
 * Created by Averox on 7/3/2017.
 */
class AccountsDataAdapter {

    private var id: Int? = null
    private var idForPost: String? = null

    private var fb_id: Int? = null
    private var name: String? = null
    private var picture: String? = null
    private var access_token: String? = null
    private var pages_posts_count: Int? = null
    private var username: String? = null
    private var display_name: String? = null
    private var password: String? = null
    private var token: String? = null
    private var group_id: Int? = null
    private var group_name: String? = null
    private var group_privacy: String? = null
    private var gp_circle_collection_id: String? = null
    private var company_id: Int? = null
    private var linkedin_id: Int? = null
    private var website_url: String? = null








    constructor(id: Int?, fb_id: Int?, name: String?, picture: String?, access_token: String?, pages_posts_count: Int?)
    {
        this.id = id
        this.fb_id = fb_id
        this.username = name
        this.name = name
        this.picture = picture
        this.access_token = access_token
        this.pages_posts_count = pages_posts_count

    }

    constructor(id: String?,name: String?)
    {
        this.idForPost = id
        this.name = name

    }

    constructor(id: Int?, fb_id: Int?, username: String?, display_name: String?, picture: String?)
    {
        this.id = id
        this.fb_id = fb_id
        this.display_name = display_name
        this.picture = picture
        this.username = username

    }

    constructor(fb_id: Int?, username: String?, password: String?, id: Int?, display_name: String?, picture: String?)
    {

        this.id = id
        this.fb_id= fb_id
        this.display_name = display_name
        this.picture = picture
        this.username = username
        this.password = password

    }

    constructor(fb_id: Int?, username: String?, id: Int?, token: String?, display_name: String?, picture: String?)
    {
        this.id = id
        this.fb_id = fb_id
        this.display_name = display_name
        this.picture = picture
        this.username = username
        this.token = token

    }

    constructor(id: String?,company_id: Int?,display_name: String?,token: String?)
    {
        this.idForPost = id
        this.company_id = company_id
        this.display_name = display_name
        this.token = token

    }

    constructor(id: Int?, fb_id: Int?, group_name: String?, picture: String?, access_token: String?, group_privacy: String?)
    {
        this.id = id
        this.fb_id = fb_id
        this.group_name = group_name
        this.picture = picture
        this.access_token = access_token
        this.group_privacy = group_privacy
    }

    constructor(id: Int?, gp_circle_id: String?, fb_id: Int?, name: String?)
    {
        this.id = id
        this.gp_circle_collection_id = gp_circle_collection_id
        this.fb_id = fb_id
        this.name = name
    }
    constructor(id: Int?, gp_circle_collection_id: String?, fb_id: Int?, name: String?,picture: String?)
    {
        this.id = id
        this.gp_circle_collection_id = gp_circle_collection_id
        this.fb_id = fb_id
        this.name = name
        this.picture = picture
    }



    constructor(id: Int?, linkedin_id: Int?,company_id: Int?, username: String?,  picture: String?, website_url: String?,token: String?)
    {
        this.id = id
        this.linkedin_id = linkedin_id
        this.company_id = company_id
        this.username = username
        this.picture = picture
        this.website_url = website_url
        this.token = token
    }
    constructor(id: Int?, linkedin_id: Int?,company_id: Int?, username: String?,  picture: String?, website_url: String?)
    {
        this.id = id
        this.linkedin_id = linkedin_id
        this.company_id = company_id
        this.username = username
        this.picture = picture
        this.website_url = website_url
    }
    fun getId(): Int? {
        return id
    }
    fun getIdForPost(): String? {
        return idForPost
    }

    fun getFbid(): Int? {
        return fb_id
    }

    fun getName(): String? {
        return name
    }

    fun getPictureUrl(): String? {
        return picture
    }

    fun getAccessToken(): String? {
        return access_token
    }

    fun getPagesCount(): Int?
    {
        return pages_posts_count
    }
    fun getPostsCount(): Int?
    {
        return pages_posts_count
    }

    fun getUsername(): String?
    {
        return username
    }
    fun getDisplayName(): String?
    {
        return display_name
    }
    fun getPassword(): String?
    {
        return password
    }
    fun getToken(): String?
    {
        return token
    }
    fun getGroupPrivacy(): String?
    {
        return group_privacy
    }
    fun getGPCircleID(): String?
    {
        return gp_circle_collection_id
    }

    fun getGPCollectionID(): String?
    {
        return gp_circle_collection_id
    }
    fun getGroupID(): Int?
    {
        return group_id
    }
    fun getGroupName(): String?
    {
        return group_name
    }
    fun getLinkedinId(): Int?
    {
        return linkedin_id
    }
    fun getCompanyId(): Int?
    {
        return company_id
    }
    fun getWebsiteUrl(): String?
    {
        return website_url
    }


}