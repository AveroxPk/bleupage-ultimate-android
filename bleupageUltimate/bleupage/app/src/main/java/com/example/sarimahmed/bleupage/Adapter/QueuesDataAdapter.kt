package com.example.sarimahmed.bleupage.Adapter

import android.text.BoringLayout

/**
 * Created by Averox on 9/21/2017.
 */
class QueuesDataAdapter(queue_id: Int,user_id: Int, name: String, time: ArrayList<String>, localtime: ArrayList<String>, days: ArrayList<Int>, links: ArrayList<String>,accounts: ArrayList<String>,status: Int)
{

    private var queue_id: Int? = null
    private var user_id: Int? = null
    private var name: String? = null
    private var time: ArrayList<String>? = null
    private var localtime: ArrayList<String>? = null
    private var days: ArrayList<Int>? = null
    private var links: ArrayList<String>? = null
    private var accounts: ArrayList<String>? = null
    private var status: Int? = null
    init {
        this.queue_id = queue_id
        this.user_id = user_id
        this.name = name
        this.time = time
        this.localtime = localtime
        this.days = days
        this.links = links
        this.accounts = accounts
        this.status = status
    }

    fun getQueueId(): Int?
    {
        return queue_id
    }
    fun getUserId(): Int?
    {
        return user_id
    }
    fun getName(): String?
    {
        return name
    }
    fun getTime(): ArrayList<String>?
    {
        return time
    }
    fun getLocalTime(): ArrayList<String>?
    {
        return localtime
    }
    fun getDays(): ArrayList<Int>?
    {
        return days
    }
    fun getLinks(): ArrayList<String>?
    {
        return  links
    }
    fun getAccounts(): ArrayList<String>?
    {
        return accounts
    }
    fun getStatus(): Int?
    {
        return status
    }




}