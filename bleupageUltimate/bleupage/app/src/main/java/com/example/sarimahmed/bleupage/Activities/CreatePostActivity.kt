package com.example.sarimahmed.bleupage.Activities

import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.example.sarimahmed.bleupage.Application.ApplicationController
import com.example.sarimahmed.bleupage.HelperClasses.AlertManager
import com.example.sarimahmed.bleupage.HelperClasses.BuildUrl
import com.example.sarimahmed.bleupage.HelperClasses.JsonKeys

import com.example.sarimahmed.bleupage.R
import com.example.sarimahmed.bleupage.SessionManager.LoginManager
import com.example.sarimahmed.bleupage.SessionManager.UserDataManager
import org.jetbrains.anko.toast
import org.json.JSONObject

class CreatePostActivity : AppCompatActivity(), View.OnTouchListener,View.OnClickListener{


    private var button_home: ImageButton? = null
    private var loginManager: LoginManager? = null
    private var status: Boolean? = false
    private var automation: Boolean? = false
    private var ios: Boolean? = false

    private var listview_menus: ListView? = null
    private var listview_menu_items: Array<String>? = null





    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_post)

        loginManager = LoginManager(this@CreatePostActivity)

        listview_menu_items = resources.getStringArray(R.array.menu_items)




       //checkPackages()
        if(UserDataManager.user_status == true && UserDataManager.automation_status == true)
        {


            //Log.d("abc","automation not purchased in if")
            return
        }
        else
        {

            Log.d("abc","automation  purchased in else")

            //autopost = findViewById(R.id.autopost) as ImageView?

           /* autopost?.setImageDrawable(resources.getDrawable(R.drawable.autopost_default))
            autopost?.setOnTouchListener(this@CreatePostActivity)
            autopost?.setOnClickListener(this@CreatePostActivity)*/
        }

        if(UserDataManager.user_status == true && UserDataManager.content_fetcher_status == true)
        {


            //Log.d("abc","automation not purchased in if")
            return
        }
        else
        {

            Log.d("abc","content fetcher in else")


          /*  content_fetcher = findViewById(R.id.content_fetcher) as ImageView?
            content_fetcher?.setImageDrawable(resources.getDrawable(R.drawable.content_fetcher_default))

            content_fetcher?.setOnTouchListener(this@CreatePostActivity)
        content_fetcher?.setOnClickListener(this@CreatePostActivity)*/
        }




/*

        general_post = findViewById(R.id.general_post) as ImageView?
        general_post?.setOnClickListener(this)
        general_post?.setOnTouchListener(this)
        discount_coupon = findViewById(R.id.discount_coupon) as ImageView?
        discount_coupon?.setOnTouchListener(this)
        discount_coupon?.setOnClickListener(this)
*/


        button_home = findViewById(R.id.button_home)
        button_home?.setOnClickListener(this)
        listview_menus = findViewById(R.id.listview_menus)
        var menulistAdapter = MenuListAdapter(this)
        listview_menus!!.adapter = menulistAdapter



        listview_menus?.setOnItemClickListener { adapterView, view, position, l ->


            if(position == 0)
            {
                val intent = Intent(this, GeneralPost::class.java)
                intent.putExtra("TITLE",resources.getString(R.string.string_toolbarTitle_generalPost))
                startActivity(intent)
                overridePendingTransition(R.anim.enter_horizontal, R.anim.exit_horizontal)
            }
            else if(position == 1)
            {
                val intent = Intent(this, Templates_Discount_Post::class.java)
                intent.putExtra("TITLE",resources.getString(R.string.string_toolbarTitle_discountPost))
                startActivity(intent)
                overridePendingTransition(R.anim.enter_horizontal, R.anim.exit_horizontal)
            }
            else if(position == 2)
            {
                val intent = Intent(this, Categories::class.java)
                intent.putExtra("FROM","createpost")
                startActivity(intent)
                overridePendingTransition(R.anim.enter_horizontal, R.anim.exit_horizontal)
            }
            else if(position == 3)
            {
                val intent = Intent(this, AddNewBlog::class.java)
                startActivity(intent)
                overridePendingTransition(R.anim.enter_horizontal, R.anim.exit_horizontal)
            }
        }

    }

    override fun onBackPressed() {
        super.onBackPressed()
        overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left)
    }
    override fun onTouch(v: View?, event: MotionEvent?): Boolean {
        var id =v?.id

        when(id)
        {

        }

        return super.onTouchEvent(event)
    }

    override fun onClick(v: View?) {
        var id = v?.id

        when(id)
        {
            R.id.button_home ->
            {
                onBackPressed()
                overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left)
            }

        }
    }


    internal inner class MenuListAdapter(private val context: Context) : BaseAdapter() {
        var listview_enteries: Array<String>
        var images = intArrayOf(R.drawable.general_post, R.drawable.discount_coupons, R.drawable.autopost, R.drawable.content_fetcher)

        init {
            listview_enteries = context.resources.getStringArray(R.array.menu_items)
        }

        override fun getCount(): Int {
            return listview_enteries.size
        }

        override fun getItem(position: Int): Any {
            return listview_enteries[position]
        }

        override fun getItemId(position: Int): Long {
            return position.toLong()
        }

        override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {

            var row: View? = null
            if (convertView == null) {
                val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
                row = inflater.inflate(R.layout.createpost_list_item, parent, false)
            } else {
                row = convertView
            }
            val drawer_item_text = row!!.findViewById<TextView>(R.id.textview_menu)
            val drawer_item_icon = row.findViewById<ImageView>(R.id.menu_icon)
            drawer_item_text.text = listview_enteries[position]
            try {
                drawer_item_icon.setImageResource(images[position])
            } catch (e: Exception) {
                //e.printStackTrace()
                Log.d("icon", e.message)
            }

            return row
        }
    }






}
