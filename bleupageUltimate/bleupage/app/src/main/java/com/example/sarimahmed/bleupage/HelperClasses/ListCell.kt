package com.example.sarimahmed.bleupage.HelperClasses

import android.R.attr.category
import android.R.attr.name



/**
 * Created by Averox on 10/13/2017.
 */

class ListCell(val name: String,val id: Int, val category: String) : Comparable<ListCell> {


    var isSectionHeader: Boolean = false
        private set

    init {
        isSectionHeader = false

    }

    fun getSectionHeader(): Boolean {
        return isSectionHeader
    }


    fun setToSectionHeader() {
        isSectionHeader = true
    }
    override fun compareTo(other: ListCell): Int {
        return this.category.compareTo(other!!.category)
    }


}
