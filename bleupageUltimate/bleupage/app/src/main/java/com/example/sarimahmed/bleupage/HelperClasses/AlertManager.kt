package com.example.sarimahmed.bleupage.HelperClasses

import android.content.Context
import android.os.Handler
import android.os.HandlerThread
import android.support.v7.app.AlertDialog
import com.example.sarimahmed.bleupage.HelperClasses.Constants.context
import android.R.attr.button
import android.os.Looper
import android.os.Message
import android.widget.TextView
import android.view.LayoutInflater
import android.widget.Button
import com.example.sarimahmed.bleupage.R
import java.util.*


/**
 * Created by Averox on 6/23/2017.
 */

class AlertManager(alertMessage :String,time: Long, context: Context)  {



    init {


        val mHandler = Handler(Looper.getMainLooper())
        mHandler.post {
            // Your UI updates here
            val dialog = AlertDialog.Builder(
                    context)
            dialog.setMessage(alertMessage)
            dialog.setCancelable(true)
            val dlg = dialog.create()
            dlg.show()
            val timer = Timer()
            timer.schedule(object : TimerTask() {
                override fun run() {
                    dlg.dismiss()
                    timer.cancel() //this will cancel the timer of the system
                }
            }, time)
        }
    }

}